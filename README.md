## This project we have a react application to return us about COVID status.

## Tools
- React
- Docker
- Docker-compose
- Jenkins
- Ansible
- Oracle Cloud (OCI)

![TOPOLOGY](https://i.ibb.co/zn97H4m/Untitled-1.jpg)

## [NOTE]:
- If you need to access the machine, please open the Oracle Cloud dashboard, there you can manager your machines. [https://www.oracle.com/cloud/sign-in.html]
- The deploy process will be do throug Jenkins. You can access the Jenkins with this link [machine_IP:8080]
## Installation
- git clone https://github.com/sambreen27/covid19.git
- cd covid19
- npm install
- npm start

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## COVID-19 API - Test

GET https://covid19.mathdro.id/api -> global cases information

GET https://covid19.mathdro.id/api/countries -> countries that are available in the API

GET https://covid19.mathdro.id/api/countries/{countryName} -> get specific country's covid-19 cases data

API provided by: https://github.com/mathdroid/covid-19-api


## Tech Stack

- [React JS](https://github.com/facebook/react)
- [Axios](https://github.com/axios/axios)
- [Charts.js](https://github.com/chartjs/Chart.js)
- [material-ui](https://github.com/mui-org/material-ui)
- [react-particles-js](https://github.com/Wufe/react-particles-js)
- [react-countup](https://github.com/glennreyes/react-countup)












